using Autofac;
using Test.Robomed.Data.DB;
using Test.Robomed.Data.File;
using Test.Robomed.Data.UpdateData;

namespace Test.Robomed.App.ViewModel
{
    public class Bootstrapper
    {
        public Bootstrapper()
        {
            ConfigureContainer();
        }

        public IContainer Container { get; set; }

        private void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Repository>()
                .As<IRepository>();
            builder.RegisterType<CsvFileHandler>()
                .As<IFileHandler>().WithParameter("filePath", "clients.csv");
            builder.RegisterType<UpdateController>();
            builder.RegisterType<MainViewModel>();
            Container = builder.Build();
        }
    }
}