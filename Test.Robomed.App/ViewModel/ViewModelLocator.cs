using Autofac;

namespace Test.Robomed.App.ViewModel
{
    /// <summary>
    ///     This class contains static references to all the view models in the
    ///     application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        private static readonly Bootstrapper bootStrapper;

        static ViewModelLocator()
        {
            if (bootStrapper == null)
                bootStrapper = new Bootstrapper();
        }

        public MainViewModel Main => bootStrapper.Container.Resolve<MainViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}