﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Test.Robomed.Data.DB;
using Test.Robomed.Data.File;

namespace Test.Robomed.Data.UpdateData
{
    public class UpdateController
    {
        private const int TimeUpdatedDatabaseMilliseconds = 5000;
        private readonly IFileHandler _fileHandler;

        private readonly Queue<ClientChangedMessage> ChangedMessageQueue = new Queue<ClientChangedMessage>();

        private readonly IRepository repository;

        public UpdateController(IRepository initRepository, IFileHandler fileHandler)
        {
            _fileHandler = fileHandler;
            Messenger.Default.Register<ClientChangedMessage>(this,
                clientChangedMessage =>
                {
                    if (IsMessageCorrect(clientChangedMessage))
                        ChangedMessageQueue.Enqueue(clientChangedMessage);
                });
            repository = initRepository;
            var observable = Observable.Interval(TimeSpan.FromMilliseconds(TimeUpdatedDatabaseMilliseconds));
            observable.Subscribe(d =>
            {
                UpdateDb();
            });
        }

        public void ReadFromFile()
        {
            try
            {
                var stringStream = Observable.Create(
                    (IObserver<Client> observer) =>
                    {
                        var cancel = new CancellationDisposable();
                        NewThreadScheduler.Default.Schedule(() =>
                        {
                            _fileHandler.ReadByBatchFile(observer);
                            observer.OnCompleted();
                        });

                        return cancel;
                    });

                stringStream.ObserveOn(Dispatcher.CurrentDispatcher)
                    .Subscribe(client => { NewClientRead?.Invoke(client); });
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception);
            }
        }

        public void WriteFile(IEnumerable<Client> clients)
        {
            var observable =
                Observable.Start(() => { _fileHandler.WriteFile(clients); }).Finally(() => WriteFinished?.Invoke());
        }

        public event Action<Client> NewClientRead;
        public event Action WriteFinished;


        private bool IsMessageCorrect(ClientChangedMessage changedMessage)
        {
            return changedMessage.Client != null;
        }

        public List<IClient> LoadClientsFromDatabase()
        {
            return repository.GetClientList().Cast<IClient>().ToList();
        }

        private void UpdateDb()
        {
            if (!ChangedMessageQueue.Any())
                return;
            try
            {
                while (ChangedMessageQueue.Count > 0)
                {
                    var item = ChangedMessageQueue.Dequeue();
                    switch (item.ClientChangedType)
                    {
                        case ClientChangedType.ClientRemoved:
                            repository.Delete(new Client(item.Client));
                            break;
                        case ClientChangedType.ClientCreatedOrUpdated:
                            CreateOrUpdateClientDb(item);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                repository.Save();
            }
            catch (Exception exception)
            {
                // TODO add logger
                Debug.WriteLine(exception);
            }
        }

        private void CreateOrUpdateClientDb(ClientChangedMessage item)
        {
            var client = repository.GetClient(new Client(item.Client));
            if (client != null)
            {
                // client != null mean that it is operation of updating
                client.BirthDate = item.Client.BirthDate;
                client.Name = item.Client.Name;
                client.Email = item.Client.Email;
                client.Telephone = item.Client.Telephone;
                repository.UpdateClient(client);
            }
            else
            {
                repository.CreateClient(new Client(item.Client));
            }
        }
    }
}