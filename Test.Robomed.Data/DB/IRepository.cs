using System;
using System.Collections.Generic;

namespace Test.Robomed.Data.DB
{
    /// <summary>
    ///     Interface for future implementation of repository pattern
    /// </summary>
    public interface IRepository : IDisposable
    {
        IEnumerable<Client> GetClientList();
        Client GetClient(Client client);
        void CreateClient(Client item);
        void UpdateClient(Client item);
        void Delete(Client client);
        void Save();
    }
}