using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using Test.Robomed.Data;
using Test.Robomed.Data.UpdateData;

namespace Test.Robomed.App.ViewModel
{
    /// <summary>
    ///     This class contains properties that the main View can data bind to.
    ///     <para>
    ///         Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    ///     </para>
    ///     <para>
    ///         You can also use Blend to data bind with the tool's support.
    ///     </para>
    ///     <para>
    ///         See http://www.galasoft.ch/mvvm
    ///     </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly UpdateController _updateController;
        private EditEventBindingList<ClientViewModel> _clients = new EditEventBindingList<ClientViewModel>();

        /// <summary>
        ///     Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(UpdateController updateController)
        {
            _updateController = updateController;
            Clients.BeforeItemWillBeRemoved += ClientsOnBeforeItemWillBeRemoved;
            OpenCommand = new RelayCommand(Open);
        }

        public EditEventBindingList<ClientViewModel> Clients
        {
            get { return _clients; }
            set
            {
                if (_clients.Equals(value))
                    return;
                _clients = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     TODO throttling
        /// </summary>
        public RelayCommand OpenCommand { get; private set; }

        private void ClientsOnBeforeItemWillBeRemoved(ClientViewModel clientViewModel)
        {
            Messenger.Default.Send(new ClientChangedMessage(clientViewModel, ClientChangedType.ClientCreatedOrUpdated));
        }

        private void Open()
        {
            // TODO change it to mvvm approach
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Tale Files|*.csv",
                InitialDirectory = AppDomain.CurrentDomain.BaseDirectory,
                Title = "Please select csv file."
            };
            if (openFileDialog.ShowDialog() != true) return;

            _updateController.ReadFromFile();
            _updateController.NewClientRead += OnNewClientRead;
        }

        private void OnNewClientRead(IClient client)
        {
            Messenger.Default.Send(new ClientChangedMessage(client, ClientChangedType.ClientCreatedOrUpdated));
            Clients.Add(new ClientViewModel(client));
        }
    }
}