﻿namespace Test.Robomed.Data
{
    public class Client : IClient
    {
        public Client()
        {
            
        }
        public Client(IClient client)
        {
            Name = client.Name;
            BirthDate = client.BirthDate;
            Email = client.Email;
            Telephone = client.Telephone;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }

        public string Email { get; set; }
        public string Telephone { get; set; }


        #region IEqualityMembers

        protected bool Equals(Client other)
        {
            return string.Equals(Name, other.Name) && string.Equals(BirthDate, other.BirthDate) &&
                   string.Equals(Email, other.Email) && string.Equals(Telephone, other.Telephone);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Client) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Name != null ? Name.GetHashCode() : 0;
                hashCode = (hashCode*397) ^ (BirthDate != null ? BirthDate.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Telephone != null ? Telephone.GetHashCode() : 0);
                return hashCode;
            }
        }

        #endregion
    }
}