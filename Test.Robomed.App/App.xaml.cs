﻿using System;
using System.Diagnostics;
using System.Windows;

namespace Test.Robomed.App
{
    /// <summary>
    ///     Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledExceptionHandler;

            base.OnStartup(e);
        }


        private void AppDomainUnhandledExceptionHandler(object sender,
            UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            // TODO change to logger
            Debug.WriteLine("Unhandled Exception {0}", unhandledExceptionEventArgs);
        }
    }
}