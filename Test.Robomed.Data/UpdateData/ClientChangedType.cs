namespace Test.Robomed.Data
{
    public enum ClientChangedType
    {
        ClientCreatedOrUpdated,
        ClientRemoved
    }
}