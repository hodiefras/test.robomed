using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Test.Robomed.Data.DB
{
    public class Repository : IRepository
    {
        private readonly MarsFlightContext _marsFlightContext;

        private bool disposed;

        public Repository()
        {
            _marsFlightContext = new MarsFlightContext();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public IEnumerable<Client> GetClientList()
        {
            return _marsFlightContext.Clients;
        }

        public Client GetClient(Client client)
        {
            return _marsFlightContext.Clients.FirstOrDefault(cl => cl.Id == client.Id);
        }

        public void CreateClient(Client item)
        {
            _marsFlightContext.Clients.Add(item);
        }

        public void UpdateClient(Client item)
        {
            _marsFlightContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Client deleteClient)
        {
            var client = _marsFlightContext.Clients.FirstOrDefault(cl => cl.Id == deleteClient.Id);
            if (client != null)
                _marsFlightContext.Clients.Remove(client);
        }

        public void Save()
        {
            _marsFlightContext.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _marsFlightContext.Dispose();
                }
            }
            disposed = true;
        }
    }
}