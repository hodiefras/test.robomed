﻿using System;
using System.ComponentModel;

namespace Test.Robomed.App.ViewModel
{
    /// <summary>
    ///     New remove event for binding list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EditEventBindingList<T> : BindingList<T>
    {
        protected override void RemoveItem(int itemIndex)
        {
            var deletedItem = Items[itemIndex];
            BeforeItemWillBeRemoved?.Invoke(deletedItem);
            base.RemoveItem(itemIndex);
        }

        public event Action<T> BeforeItemWillBeRemoved;
    }
}