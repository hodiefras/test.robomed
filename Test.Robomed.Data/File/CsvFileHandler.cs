using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace Test.Robomed.Data.File
{
    public class CsvFileHandler : IFileHandler
    {
        private readonly string _filePath;

        public CsvFileHandler(string filePath)
        {
            _filePath = filePath;
        }

        public void ReadByBatchFile(IObserver<Client> observer)
        {
            using (var sr = new StreamReader(_filePath))
            {
                var config = new CsvConfiguration();
                config.RegisterClassMap<ClientCsvMap>();
                using (var csv = new CsvReader(sr, config))
                {
                    while (csv.Read())
                    {
                        var client = csv.GetRecord<Client>();
                        // on next for row-reading
                        observer.OnNext(client);
                    }
                }
            }
        }

        public void WriteFile(IEnumerable<Client> clients)
        {
            using (var sr = new StreamWriter(_filePath))
            {
                var config = new CsvConfiguration();
                config.RegisterClassMap<ClientCsvMap>();
                using (var csv = new CsvWriter(sr, config))
                {
                    csv.WriteRecords(clients);
                }
            }
        }
    }
}