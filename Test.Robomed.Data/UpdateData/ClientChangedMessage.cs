using GalaSoft.MvvmLight.Messaging;

namespace Test.Robomed.Data
{
    public class ClientChangedMessage : MessageBase
    {
        public ClientChangedMessage(IClient client, ClientChangedType changedType)
        {
            Client = client;
            ClientChangedType = changedType;
        }

        public ClientChangedType ClientChangedType { get; private set; }
        public IClient Client { get; private set; }
    }
}