﻿namespace Test.Robomed.Data
{
    public interface IClient
    {
        string Name { get; set; }
        string BirthDate { get; set; }

        string Email { get; set; }
        string Telephone { get; set; }
    }
}