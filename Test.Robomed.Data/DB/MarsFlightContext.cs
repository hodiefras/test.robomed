using System.Data.Entity;

namespace Test.Robomed.Data.DB
{
    public class MarsFlightContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
    }
}