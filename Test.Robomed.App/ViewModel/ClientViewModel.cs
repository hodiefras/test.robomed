﻿using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Test.Robomed.Data;

namespace Test.Robomed.App.ViewModel
{
    public class ClientViewModel : ViewModelBase, IEditableObject, IClient
    {
        public ClientViewModel()
        {
        }

        public ClientViewModel(IClient client)
        {
            Name = client.Name;
            BirthDate = client.BirthDate;
            Email = client.Email;
            Telephone = client.Telephone;
        }

        public string Name { get; set; }
        public string BirthDate { get; set; }

        public string Email { get; set; }
        public string Telephone { get; set; }

        #region Implement IEditable

        public void BeginEdit()
        {
        }

        public void EndEdit()
        {
            Messenger.Default.Send(new ClientChangedMessage(this, ClientChangedType.ClientCreatedOrUpdated));
        }

        public void CancelEdit()
        {
        }

        #endregion
    }
}