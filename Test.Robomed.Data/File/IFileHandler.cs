using System;
using System.Collections.Generic;

namespace Test.Robomed.Data.File
{
    public interface IFileHandler
    {
        void ReadByBatchFile(IObserver<Client> observer);
        void WriteFile(IEnumerable<Client> clients);
    }
}