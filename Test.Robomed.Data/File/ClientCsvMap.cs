using CsvHelper.Configuration;

namespace Test.Robomed.Data.File
{
    internal sealed class ClientCsvMap : CsvClassMap<Client>
    {
        public ClientCsvMap()
        {
            Map(m => m.Name).Name("Name");
            Map(m => m.BirthDate).Name("BirthDate");
            Map(m => m.Email).Name("Email");
            Map(m => m.Telephone).Name("Telephone");
        }
    }
}